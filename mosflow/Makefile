ifeq ($(shell uname -s),Darwin)
	OPENSCAD := /Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD
else ifeq ($(shell uname -s),Linux)
	OPENSCAD := /usr/bin/openscad
endif


.PRECIOUS: cache/%.stl cache/%-lores.stl cache/%-midres.stl cache/%-hires.stl

SHELL := bash

INDENT := 2>&1 | sed 's/^/    /'

make.targets :
	@echo "available Make targets:"
	@$(MAKE) -pRrq -f $(firstword $(MAKEFILE_LIST)) : 2>/dev/null \
	| awk -v RS= -F: '/^# Implicit Rules/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' \
	| egrep -v '^make.targets$$' \
	| sed 's/^/    make    /' \
	| env LC_COLLATE=C sort


include $(shell find . -name '*.deps')


diag :
	uname -a
	uname -s
	env | sort

clean :
	find * -type f -name '*~' -exec rm -v {} \; ${INDENT}
	find * -type f -name '*.stl' -exec rm -v {} \; ${INDENT}
	find * -name '*.deps' -exec rm -v {} \; ${INDENT}
	([ -d artifacts ] && rmdir artifacts || true) ${INDENT}
	([ -d cache ] && rmdir cache || true) ${INDENT}

tidy :
	find artifacts -type f -name '*.stl' -exec rm -v {} \; ${INDENT}
	([ -d artifacts ] && rmdir artifacts || true) ${INDENT}

cache/%.stl : %.scad
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d $@.deps -o $@ $<
	date
	@echo .
	@echo .
	@echo .


cache/%-lores.stl : %.scad %.json
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d cache/$*.stl.deps -o $@ -p $*.json -P lores $<
	date
	@echo .
	@echo .
	@echo .



cache/%-midres.stl : %.scad %.json
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d cache/$*.stl.deps -o $@ -p $*.json -P midres $<
	date
	@echo .
	@echo .
	@echo .



cache/%-hires.stl : %.scad %.json
	[ -d cache ] || mkdir -v cache
	date
	time ${OPENSCAD} -d cache/$*.stl.deps -o $@ -p $*.json -P hires $<
	date
	@echo .
	@echo .
	@echo .




artifacts/%.stl : cache/%.stl
	@[ -d artifacts ] || mkdir -v artifacts
	cp $< $@

all-really : all allall

everything all : stl


allall : stl-lores stl-midres stl-hires


foo :
	for profile in *.json ; do \
		profile=$$(basename $$profile .json) ; \
		cat $${profile}.json | \
			jq -r '.parameterSets | keys[]' | \
			while read paramset ; do \
				echo doing $${paramset} for $${profile} ; \
			done ; \
	done


stl :
	@grep --files-without-match NOSTL *scad | while read s ; do \
		[ -r $$s ] && ${MAKE} --no-print-directory ${MAKEFLAGS} artifacts/$$(basename $$s .scad).stl ; \
	done

stl-% :
	@grep --files-without-match NOSTL *scad | while read s ; do \
		profile=$$(basename $$s .scad) ; \
		echo checking $$s against $${profile}.json ; \
		if [ -r $${profile}.json ] ; then \
			echo hooray ; \
			cat $${profile}.json | \
				jq -r '.parameterSets | keys[]' | fgrep $* | \
				while read paramset ; do \
					echo doing $${paramset} for $${profile} ; \
					${MAKE} --no-print-directory ${MAKEFLAGS} artifacts/$${profile}-$${paramset}.stl ; \
				done ; \
		fi ; \
	done


FLOW_SOURCE         := mosflow.yml
FLOW_YML_TO_JSON    := cat ${FLOW_SOURCE} | python3 -c 'import json, sys, yaml ; y=yaml.safe_load(sys.stdin.read()) ; json.dump(y, sys.stdout)'
FLOW_SUBTREES       := ${FLOW_YML_TO_JSON} | jq -r '.subtrees[]? | select(.active == "true")'
FLOW_SUBMODULES     := ${FLOW_YML_TO_JSON} | jq -r '.submodules[]? | select(.active == "true")'
FLOW_NOT_SUBMODULES := ${FLOW_YML_TO_JSON} | jq -r '.submodules[]? | select(.active == "false")'




FLOW_SUBTREE_LIST := ${FLOW_SUBTREES} | jq -r '"subtree \"\(.nickname)\" is configured at (prefix repo branch): (\(.config.prefix) \(.config.repo) \(.config.branch))"'
mosflow.subtree-list : ${FLOW_SOURCE}
	@${FLOW_SUBTREE_LIST}


FLOW_SUBTREE_ADD  := ${FLOW_YML_TO_JSON} | jq -r '.subtrees[] | select(.active == "true") | .config | "[ -d \(.prefix) ] || git subtree add  --squash --prefix=\(.prefix) \(.repo) \(.branch)"'
mosflow.subtree-add : ${FLOW_SOURCE}
	@${FLOW_SUBTREE_ADD}
mosflow.subtree-add! : ${FLOW_SOURCE}
	@${FLOW_SUBTREE_ADD} | sh


mosflow.json : mosflow.yml
	@cat $< | python3 -c 'import json, sys, yaml ; y=yaml.safe_load(sys.stdin.read()) ; json.dump(y, sys.stdout)' | jq . > $@

FLOW_SUBTREE_PULL := ${FLOW_YML_TO_JSON} | jq -r '.subtrees[] | select(.active == "true") | .config | "[ -d \(.prefix) ] && git subtree pull --squash --prefix=\(.prefix) --message \"subtree pull merge\" \(.repo) \(.branch)"'
mosflow.subtree-pull : ${FLOW_SOURCE}
	@${FLOW_SUBTREE_PULL}
mosflow.subtree-pull! : ${FLOW_SOURCE}
	@${FLOW_SUBTREE_PULL} | sh


mosflow.bootstrap mosflow.yml :
	@echo "---"                                            >  mosflow.yml
	@echo "mosflow-version:"                               >> mosflow.yml
	@echo "  version: '1.0'"                               >> mosflow.yml
	@echo "subtrees:"                                      >> mosflow.yml
	@echo "- nickname: mosflow"                            >> mosflow.yml
	@echo "  active: 'true'"                               >> mosflow.yml
	@echo "  config:"                                      >> mosflow.yml
	@echo "    prefix: mosflow"                            >> mosflow.yml
	@echo "    repo: git@gitlab.com:openscad1/mosflow.git" >> mosflow.yml
	@echo "    branch: v0-lts"                             >> mosflow.yml
	@echo ""                                               >> mosflow.yml
	echo "created bootstrap mosflow.yml"

mosflow.gitignore :
	@[ -f .gitignore ]                                   || { touch .gitignore                 ; echo "created .gitignore" ; }
	@[ -f .gitignore ] && grep --silent "^*~"          .gitignore || { echo "*~"          >> .gitignore ; echo "added *~ to .gitignore" ; }
	@[ -f .gitignore ] && grep --silent "^artifacts/*" .gitignore || { echo "artifacts/*" >> .gitignore ; echo "added artifacts/* to .gitignore" ; }
	@[ -f .gitignore ] && grep --silent "^cache/*"     .gitignore || { echo "cache/*"     >> .gitignore ; echo "added cache/* to .gitignore" ; }


#EOF
