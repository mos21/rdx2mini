# **M**ichael's **O**pen**S**cad git work**flow**

## Install

    [ -d mosflow ]        || git subtree add --squash --prefix=mosflow git@gitlab.com:openscad1/mosflow.git v0-lts

    [ -f Makefile ]       || { echo "include mosflow/Makefile" >> Makefile ; }

    [ -f mosflow.yml ]    || { make mosflow.bootstrap ; }

    [ -f .gitlab-ci.yml ] || { echo "include: 'mosflow/dot-gitlab-ci.yml'" >> .gitlab-ci.yml ; }

