include <moslib/libchamfer.scad>;
include <peglib/defaults.scad>;
include <peglib/rail.scad>;


inch = 25.4;
wall_thickness = inch / 8;
wall_bevel = wall_thickness / 4;


align_up = [0, 0, 1];

slug_size = [91, 100.5 - 4, 125];


tray_front_height = inch * 2;
tray_back_height = inch * 5;

module slug() {
    translate([0, 0, wall_thickness])
    color("red") {
        chamfered_box(slug_size, align = align_up, chamfer = wall_bevel * 2);
    }
    
    
    
    for(x = [-20, 20]) { 
        translate([x, slug_size.y * -(1/2) - 10, 20]) {
            rotate([-90, 0, 0]) {
                xt60();
            }
        }
    }
    
    
    translate([0 - 16, slug_size.y / 2 + 21, inch + 8 + wall_thickness]) {
        rotate([90, 0, 0]) {
            ac(); 
        }
    }



}


module xt60() {
    plug_width = 8.1;
    plug_height = 15.5;
    offset = plug_width / 4;
    plug_tallness = 20.2;
    stump_tallness = 7.8;
    
    module pluggy(height) {
        translate([plug_width * -(1/2), plug_height * -(1/2), 0]) {
            linear_extrude(height = height) {
                polygon(points=[
                    [0,0], 
                    [plug_width,0],
                    [plug_width, plug_height - offset],
                    [plug_width - offset, plug_height],
                    [offset, plug_height],
                    [0, plug_height - offset],
                    [0,0]
                ]);
            }
        }
    }
    
    pluggy(height = stump_tallness);
    scale([0.9, 0.9, 1]) {
        pluggy(height = plug_tallness);
    }
    for(y = [plug_height / 4 * -1, plug_height / 4]) {
        translate([0, y, 0]) {
            $fn = 100;
            cylinder(d = 3.5, h = 10, center = true);
        }
    }

}


module ac() {
      
    $fn = 100;

    barrel_diameter = 8;
    plug_height = 16;
    plug_tallness = 31;
    
    hull() {
        for(y = [-plug_height / 4, plug_height / 4]) {
            translate([0, y, 0]) {  

                cylinder(d = barrel_diameter, plug_tallness/2);
            }
        }
    }
    translate([0, 0, plug_tallness / 2]) {
        cube([barrel_diameter / 3, barrel_diameter / 2, plug_tallness], center = true);
    }
    for(y = [-plug_height / 4, plug_height / 4]) {
        translate([0, y, 0]) {   

            cylinder(d = barrel_diameter, plug_tallness);
        }
    }
    translate([0, 0, plug_tallness / 2]) {
        cube([barrel_diameter / 3, barrel_diameter / 2, plug_tallness], center = true);
    }
    translate([0, 0, barrel_diameter / 2 + 3])
    rotate([0, 90, 0]) {
        hull() {
            for(y = [-11/4 + barrel_diameter / 2, 11/4 - barrel_diameter / 2]) {
                translate([0, y, 0]) {
                    cylinder(d = barrel_diameter, plug_tallness);
                }
            }
        }
    }
    
}








module tray() {
    
    color("steelblue") {

        // front wall
        translate([0, -(slug_size.y / 2) - (wall_thickness / 2), 0]) {
            chamfered_box([slug_size.x + (wall_thickness * 2), wall_thickness, tray_front_height / 6], align_up, chamfer = wall_bevel);
        }
        for(x = [-1, 1]) {
            hull() {

                translate([x * ((slug_size.x / 2) + (wall_thickness / 2)), -((slug_size.y / 2) + (wall_thickness / 2)), 0]) {
                    chamfered_box([wall_thickness, wall_thickness, tray_front_height], align = align_up, chamfer = wall_bevel);
                } 

                translate([x * (3/4) * ((slug_size.x / 2) + (wall_thickness / 2)), -((slug_size.y / 2) + (wall_thickness / 2)), 0]) {
                    chamfered_box([wall_thickness, wall_thickness, tray_front_height / 1.25], align = align_up, chamfer = wall_bevel);
                }

                translate([x * (3/4) * ((slug_size.x / 2) + (wall_thickness / 2)), -((slug_size.y / 2) + (wall_thickness / 2)), 0]) {
                    chamfered_box([wall_thickness, wall_thickness, tray_front_height / 2], align = align_up, chamfer = wall_bevel);
                }
            }
            hull() {

                translate([(x * (3/4)) * ((slug_size.x / 2) + (wall_thickness / 2)), -((slug_size.y / 2) + (wall_thickness / 2)), 0]) {
                    chamfered_box([wall_thickness, wall_thickness, tray_front_height / 3], align = align_up, chamfer = wall_bevel);
                }

                translate([(x * (5/8)) * ((slug_size.x / 2) + (wall_thickness / 2)), -((slug_size.y / 2) + (wall_thickness / 2)), 0]) {
                    chamfered_box([wall_thickness, wall_thickness, tray_front_height / 6], align = align_up, chamfer = wall_bevel);
                }
            }
        }


        // back wall
        translate([0, (slug_size.y / 2) + (wall_thickness / 2), 0]) {
            chamfered_box([slug_size.x + (wall_thickness * 2), wall_thickness, inch * (3/4)], align_up, chamfer = wall_bevel);
        }
        translate([0, (slug_size.y / 2) + (wall_thickness / 2), inch * 2]) {
            chamfered_box([slug_size.x + (wall_thickness * 2), wall_thickness, inch * 3], align_up, chamfer = wall_bevel);
        }
        for(x = [-(slug_size.x / 2 - (inch / 4)) - wall_thickness, slug_size.x / 2 - (inch / 4) + wall_thickness]) {
            translate([x, (slug_size.y / 2) + (wall_thickness / 2), 0]) {
                chamfered_box([inch * (1/2), wall_thickness, inch * 5], align_up, chamfer = wall_bevel);
            }
        }    
        
        
        // side walls
        for(x = [-1, 1]) {
            hull() {
                translate([x * ((slug_size.x / 2) + (wall_thickness / 2)), -((slug_size.y / 2) + (wall_thickness / 2)), 0]) {
                    chamfered_box([wall_thickness, wall_thickness, tray_front_height], align = align_up, chamfer = wall_bevel);
                }

                translate([x * ((slug_size.x / 2) + (wall_thickness / 2)),  ((slug_size.y / 2) + (wall_thickness / 2)), 0]) {
                    chamfered_box([wall_thickness, wall_thickness, tray_back_height], align = align_up, chamfer = wall_bevel);
                }
            }
        }
        
        // floor
        chamfered_box([slug_size.x + (wall_thickness * 2), slug_size.y + (wall_thickness * 2), wall_thickness], align = align_up, chamfer = wall_bevel);
        //floor interior chamfers
        difference() {
            chamfered_box([slug_size.x + (wall_thickness * 2), slug_size.y + (wall_thickness * 2), wall_thickness * 2], align = align_up, chamfer = wall_bevel);
            translate([0, 0, wall_thickness]) {
                chamfered_box([slug_size.x, slug_size.y + (wall_thickness * 4), wall_thickness * 2], align = align_up, chamfer = wall_bevel);
            }
        }
        difference() {
            chamfered_box([slug_size.x + (wall_thickness * 2), slug_size.y + (wall_thickness * 2), wall_thickness * 2], align = align_up, chamfer = wall_bevel);
            translate([0, 0, wall_thickness]) {
                chamfered_box([slug_size.x + (wall_thickness * 4), slug_size.y , wall_thickness * 2], align = align_up, chamfer = wall_bevel);
            }
        }


        
    }
}






//slug();




tray();
color("orange")
translate([0, slug_size.y / 2 + (inch * 1) - 0.2, inch * 2.5]) {
    rotate([0, 0, 180]) {
        rail(2, chamfer = wall_bevel, back_wall_gap = default_back_wall_gap, copies = 3);
    }
}


module plate() {
    foo = 2;
    translate([0, wall_thickness * (foo/2), inch * 0.5]) {
        chamfered_box([wall_thickness, inch + (wall_thickness * foo), inch*2], align = align_up, chamfer = wall_bevel);
    }
}
translate([0, 0, inch * 2]) {
    color("bisque") {
        for(x = [-(inch * 1.5 - (wall_thickness / 2)), (inch * 1.5 - (wall_thickness / 2))]) {
            translate([x, slug_size.y / 2 + (inch / 2), 0]) {
                plate();
            }
        }
        for(x = [-inch / 2, inch/2]) {
            translate([x, slug_size.y / 2 + (inch / 2), 0]) {
                plate();
            }
        }

    }
}

color("red") {
    translate([0, slug_size.y / 2, 0 ]) {
        chamfered_box([inch * 3, inch + wall_thickness * 2, inch/2], align = [0, 1, 1], chamfer = wall_bevel);
    }
}


